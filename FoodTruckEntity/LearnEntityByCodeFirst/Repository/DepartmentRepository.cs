﻿using LearnEntityByCodeFirst.Entities.CodeFirst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityByCodeFirst.Repository
{
    public class DepartmentRepository
    {

        public List<Department> FindAll()
        {
            using (SMContext context = new SMContext())
            {
                IQueryable<Department> lDep = from dep in context.Departments select dep;
                return lDep.ToList();
            }
        }

        public Department FindById(int _id)
        {
            using (SMContext context = new SMContext())
            {
                Department dep = context.Departments.FirstOrDefault(x => x.Id == _id);
                return dep;
            }
        }

        public Department InsertToDb(Department dep)
        {
            using (SMContext context = new SMContext())
            {
                context.Departments.Add(dep);
                context.SaveChanges();
                return dep;
            }
        }

        public void DeleteById(int _id)
        {
            using (SMContext context = new SMContext())
            {
                try
                {
                    context.Departments.Remove(FindById(_id));
                    context.SaveChanges();
                }
                catch
                {
                    throw new Exception("Impossible de supprimer ce département.");
                }

            }
        }

        public Department UpdateById(int _id, Department newDep)
        {
            using (SMContext context = new SMContext())
            {
                Department dep = context.Departments.FirstOrDefault(x => x.Id == _id);

                try
                {
                    dep.Location= newDep.Location;
                    dep.Name = newDep.Name;
                    dep.Employees = newDep.Employees;
                }
                catch
                {

                }

                context.SaveChanges();

                return dep;
            }
        }
    }
}
