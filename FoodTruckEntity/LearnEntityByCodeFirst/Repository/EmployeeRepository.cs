﻿using LearnEntityByCodeFirst.Entities.CodeFirst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityByCodeFirst.Repository
{
    public class EmployeeRepository
    {
        public List<Employee> FindAll()
        {
            using (SMContext context = new SMContext())
            {
                IQueryable<Employee> lEmp = from emp in context.Employees select emp;

                return lEmp.ToList();
            }
        }

        public Employee FindById(int _id)
        {
            using (SMContext context = new SMContext())
            {
                Employee emp = context.Employees.FirstOrDefault(x => x.Id == _id);
                return emp;
            }
        }

        public Employee InsertToDb(Employee emp)
        {
            using (SMContext context = new SMContext())
            {
                context.Employees.Add(emp);
                context.SaveChanges();
                return emp;
            }
        }

        public void DeleteById(int _id)
        {
            using (SMContext context = new SMContext())
            {
                context.Employees.Remove(context.Employees.FirstOrDefault(x => x.Id == _id));
                context.SaveChanges();
            }
        }

        public Employee UpdateById(int _id, Employee newEmp)
        {
            using (SMContext context = new SMContext())
            {
                Employee emp = context.Employees.FirstOrDefault(x => x.Id == _id);

                try
                {
                    emp.FirstName = newEmp.FirstName;
                    emp.LastName = newEmp.LastName;
                    emp.Salary = newEmp.Salary;
                    emp.Gender = newEmp.Gender;
                    emp.Department = newEmp.Department;
                }
                catch
                {

                }
                context.SaveChanges();
                return emp;
            }
        }
    }
}
