﻿using LearnEntityByCodeFirst.Entities.CodeFirst;
using LearnEntityByCodeFirst.Repository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityByCodeFirst
{
    class Program
    {
        static void Main(string[] args)
        {

            Employee e1 = new Employee()
            {
                FirstName = "Steve",
                LastName = "Mike",
                Gender = "M",
                Salary = 40000
            };

            Employee e2 = new Employee()
            {
                FirstName = "Bob",
                LastName = "Robert",
                Gender = "M",
                Salary = 30000
            };

            Employee e3 = new Employee()
            {
                FirstName = "Sylvia",
                LastName = "Plath",
                Gender = "F",
                Salary = 60000
            };

            Employee e4 = new Employee()
            {
                FirstName = "Larry",
                LastName = "Hubert",
                Gender = "M",
                Salary = 40000
            };

            Employee e5 = new Employee()
            {
                FirstName = "Roy",
                LastName = "Hugo",
                Gender = "M",
                Salary = 30000
            };

            Employee e6 = new Employee()
            {
                FirstName = "Margaret",
                LastName = "Ubu",
                Gender = "F",
                Salary = 60000
            };

            Collection<Employee> listeEmployees = new Collection<Employee>();
            listeEmployees.Add(e4);
            listeEmployees.Add(e5);
            listeEmployees.Add(e6);

            Department d1 = new Department()
            {
                Name = "AJC",
                Location = "Strasbourg",
                Employees = listeEmployees
            };

            Collection<Employee> t2 = new Collection<Employee>();
            t2.Add(e1);
            t2.Add(e2);
            t2.Add(e3);

            Department d2 = new Department()
            {
                Name = "Amazon",
                Location = "Seattle",
                Employees = t2
            };

            DepartmentRepository dRep = new DepartmentRepository();
            //dRep.InsertToDb(d1);
            //dRep.InsertToDb(d2);
            EmployeeRepository eRep = new EmployeeRepository();

            eRep.DeleteById(38);
            eRep.DeleteById(40);
            eRep.UpdateById(37, new Employee(){ FirstName = "TestUpdate1", LastName = "Stapleton", Gender = "M", Salary = 50000 });
            eRep.UpdateById(42, new Employee() { FirstName = "TestUpdate2", LastName = "Chemise", Gender = "M", Salary = 50000 });
        }
    }
}
