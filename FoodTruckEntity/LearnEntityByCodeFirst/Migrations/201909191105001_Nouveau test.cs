namespace LearnEntityByCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Nouveautest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        StreetNum = c.String(nullable: false, maxLength: 128),
                        StreetName = c.String(nullable: false, maxLength: 128),
                        Town = c.String(nullable: false, maxLength: 128),
                        PostCode = c.String(nullable: false, maxLength: 128),
                        Country = c.String(nullable: false, maxLength: 128),
                        Owner_Id = c.Int(),
                        Owner_Email = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Id, t.StreetNum, t.StreetName, t.Town, t.PostCode, t.Country })
                .ForeignKey("dbo.Users", t => new { t.Owner_Id, t.Owner_Email })
                .Index(t => new { t.Owner_Id, t.Owner_Email });
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Email = c.String(nullable: false, maxLength: 128),
                        LastName = c.String(),
                        FirstName = c.String(),
                        Dob = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        Phone = c.String(),
                        Salary = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Id, t.Email });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Addresses", new[] { "Owner_Id", "Owner_Email" }, "dbo.Users");
            DropIndex("dbo.Addresses", new[] { "Owner_Id", "Owner_Email" });
            DropTable("dbo.Users");
            DropTable("dbo.Addresses");
        }
    }
}
