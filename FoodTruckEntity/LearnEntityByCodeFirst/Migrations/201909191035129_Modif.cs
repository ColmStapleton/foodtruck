namespace LearnEntityByCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Modif : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MailServers", "IpAddress", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MailServers", "IpAddress");
        }
    }
}
