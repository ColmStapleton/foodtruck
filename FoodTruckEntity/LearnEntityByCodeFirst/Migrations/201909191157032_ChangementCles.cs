namespace LearnEntityByCodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangementCles : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Addresses", new[] { "Owner_Id", "Owner_Email" }, "dbo.Users");
            DropIndex("dbo.Addresses", new[] { "Owner_Id", "Owner_Email" });
            DropColumn("dbo.Addresses", "Owner_Email");
            RenameColumn(table: "dbo.Addresses", name: "Owner_Id", newName: "Owner_Email");
            DropPrimaryKey("dbo.Addresses");
            DropPrimaryKey("dbo.Users");
            AlterColumn("dbo.Addresses", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Addresses", "Owner_Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.Users", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Addresses", new[] { "StreetNum", "StreetName", "Town", "PostCode", "Country" });
            AddPrimaryKey("dbo.Users", "Email");
            CreateIndex("dbo.Addresses", "Owner_Email");
            AddForeignKey("dbo.Addresses", "Owner_Email", "dbo.Users", "Email");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Addresses", "Owner_Email", "dbo.Users");
            DropIndex("dbo.Addresses", new[] { "Owner_Email" });
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Addresses");
            AlterColumn("dbo.Users", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Addresses", "Owner_Email", c => c.Int());
            AlterColumn("dbo.Addresses", "Id", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Users", new[] { "Id", "Email" });
            AddPrimaryKey("dbo.Addresses", new[] { "Id", "StreetNum", "StreetName", "Town", "PostCode", "Country" });
            RenameColumn(table: "dbo.Addresses", name: "Owner_Email", newName: "Owner_Id");
            AddColumn("dbo.Addresses", "Owner_Email", c => c.String(maxLength: 128));
            CreateIndex("dbo.Addresses", new[] { "Owner_Id", "Owner_Email" });
            AddForeignKey("dbo.Addresses", new[] { "Owner_Id", "Owner_Email" }, "dbo.Users", new[] { "Id", "Email" });
        }
    }
}
