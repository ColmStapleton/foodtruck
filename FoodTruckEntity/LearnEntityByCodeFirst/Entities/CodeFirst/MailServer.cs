﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityByCodeFirst.Entities.CodeFirst
{
    public class MailServer
    {
        [Key]
        public int ID { get; set; }
        public string Host { get; set; }
        public string Username { get; set; }
        public Password Password { get; set; }
        //Virtual pour spécifier que c'est une cléf étrangère
        public string IpAddress { get; set; }
        public virtual ICollection<Sender> Senders { get; set; }
    }
}
