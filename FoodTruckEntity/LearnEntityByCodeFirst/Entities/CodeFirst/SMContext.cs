﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LearnEntityByCodeFirst.Entities.CodeFirst
{
    public class SMContext : DbContext
    {
        public DbSet<MailServer> MailServers { get; set; }
        public DbSet<Sender> Senders { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses{ get; set; }

    }
}
