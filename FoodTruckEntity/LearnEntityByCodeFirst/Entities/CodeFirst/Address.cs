﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityByCodeFirst.Entities.CodeFirst
{
    public class Address
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public User Owner { get; set; }
        [Key, Column(Order = 0)]
        public string StreetNum { get; set; }
        [Key, Column(Order = 1)]
        public string StreetName { get; set; }
        [Key, Column(Order = 2)]
        public string Town { get; set; }
        [Key, Column(Order = 3)]
        public string PostCode { get; set; }
        [Key, Column(Order = 4)]
        public string Country { get; set; }

    }
}
