﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityByCodeFirst.Entities.CodeFirst
{
    public class Password
    {
        public string Value { get; set; }
        public bool AllowSave { get; set; }
    }
}
