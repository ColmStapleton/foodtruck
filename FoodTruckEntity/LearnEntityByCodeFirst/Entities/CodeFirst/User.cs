﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearnEntityByCodeFirst.Entities.CodeFirst
{
    public class User
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public virtual ICollection<Address> MyAddresses { get; set; }
        public DateTime Dob { get; set; }
        public int Gender { get; set; }
        [Key]
        public string Email { get; set; }

        public string Phone { get; set; }

        public int Salary { get; set; }
    }
}
