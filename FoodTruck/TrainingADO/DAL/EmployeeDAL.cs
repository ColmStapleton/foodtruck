﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingADO.Models;

namespace TrainingADO.DAL
{
    public class EmployeeDAL
    {
        public Employee FindById(int _id)
        {
            Employee emp = new Employee();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = "SELECT * FROM Employees Where Id = @IdEmploye";
                    command.Parameters.AddWithValue("@IdEmploye", _id);
                    
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            emp.Id = (int)reader["Id"];
                            emp.Nom = reader["Nom"].ToString();
                            emp.Prenom = reader["Prenom"].ToString();
                            emp.Dob = (DateTime)reader["Date_naissance"];
                        }
                        else
                        {
                            Console.WriteLine("Il n'y a pas d'employé ayant l'id {0} dans la table.", _id);
                        }
                    }
                }
            }
           
            return emp;
        }

        public void DeleteById(int _id)
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = "DELETE FROM Employees Where Id = @IdEmploye";
                    command.Parameters.AddWithValue("@IdEmploye", _id);
                    int numDel = command.ExecuteNonQuery();

                    if (numDel == 0)
                    {
                        Console.WriteLine("Il n'y pas d'employé ayant l'id {0} dans la table.", _id);
                    }
                    else
                    {
                        Console.WriteLine("L'employé ayant l'id {0} a bien été supprimé de la table", _id);
                    }
                }
            }
        }

        public List<Employee> FindAll()
        {
            List<Employee> lEmp = new List<Employee>();

            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("SELECT * FROM Employees");

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Employee emp = new Employee()
                            {
                                Id = (int)reader["Id"],
                                Nom = reader["Nom"].ToString(),
                                Prenom = reader["Prenom"].ToString(),
                                Dob = (DateTime)reader["Date_naissance"]
                            };  
                            lEmp.Add(emp);
                        }
                    }
                }
            }
            return lEmp;
        }

        public void InsertToDb(Employee e)
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("INSERT INTO Employees (Nom, Prenom, Date_naissance) VALUES (@Nom,@Prenom,@Date)");
                    command.Parameters.AddWithValue("@Nom", e.Nom);
                    command.Parameters.AddWithValue("@Prenom", e.Prenom);
                    command.Parameters.AddWithValue("@Date", e.Dob);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.InsertCommand = command;
                    dataAdapter.InsertCommand.ExecuteNonQuery();                
                }
            }
        }

        public void UpdateDbById(int id, Employee e)
        {
            using (SqlConnection connect = ConnectToDB())
            {
                using (SqlCommand command = connect.CreateCommand())
                {
                    command.CommandText = String.Format("UPDATE Employees SET Nom = @Nom, Prenom = @Prenom, Date_naissance = @Date WHERE Id = @Id");
                    command.Parameters.AddWithValue("@Nom", e.Nom);
                    command.Parameters.AddWithValue("@Prenom", e.Prenom);
                    command.Parameters.AddWithValue("@Date", e.Dob);
                    command.Parameters.AddWithValue("@Id", id);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.UpdateCommand = command;
                    int num = dataAdapter.UpdateCommand.ExecuteNonQuery();

                    Console.WriteLine("{0} entrée a bien été modifiée", num);
                }
            }
        }

        private SqlConnection ConnectToDB()
        {
            SqlConnection connect = new SqlConnection();
            ConnectionStringSettings connex = ConfigurationManager.ConnectionStrings["ConnectionFoodTruck"];

            connect.ConnectionString = connex.ConnectionString;
            connect.Open();
            return connect;
        }
    }
}
