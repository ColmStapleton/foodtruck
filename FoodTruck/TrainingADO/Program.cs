﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrainingADO.DAL;
using TrainingADO.Models;

namespace TrainingADO
{
    class Program
    {
        static void Main(string[] args)
        {
            EmployeeDAL eDal = new EmployeeDAL();

            Employee modifEmployee = new Employee("Adrien", "Coucou", new DateTime(1888, 4, 13));
            //eDal.UpdateDbById(14, modifEmployee);

            Employee newEmployee = new Employee("Mamadou", "Eric", new DateTime(1955,7,4));
            eDal.InsertToDb(newEmployee);


            List<Employee> lEmp = eDal.FindAll();

            foreach (Employee employee in lEmp)
            {
                Console.WriteLine(employee.ToString());
            }
            Console.ReadLine();
        }
    }
}
