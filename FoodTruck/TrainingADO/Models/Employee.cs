﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrainingADO.Models
{
    public class Employee
    {
        private string _prenom;

        public string Prenom
        {
            get { return _prenom; }
            set { _prenom = value; }
        }

        private string _nom;

        public string Nom
        {
            get { return _nom; }
            set { _nom = value; }
        }

        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private DateTime _dob;

        public DateTime Dob
        {
            get { return _dob; }
            set { _dob = value; }
        }


        public Employee()
        {
        }

        public Employee(int id,string n,string p, DateTime d)
        {
            Id = id;
            Nom = n;
            Prenom = p;
            Dob = d;
        }

        public Employee(string n, string p, DateTime d)
        {
            Nom = n;
            Prenom = p;
            Dob = d;
        }

        public override string ToString()
        {
            return (String.Format("Id : {0}, Nom : {1}, Prénom : {2}, Date De Naissance : {3}", this.Id, this.Nom, this.Prenom, this.Dob.ToString()));
        }

    }
}
